package com.agatastasiak.product;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.agatastasiak.product.entity.Product;
import com.agatastasiak.product.repository.ProductRepository;
import com.agatastasiak.product.service.ProductService;
import com.agatastasiak.product.service.impl.ProductServiceImpl;

@RunWith(SpringRunner.class)
//@DataJpaTest
public class ProductRepositoryTests {
	
	@TestConfiguration
    static class ProductServiceImplTestContextConfiguration {
  
        @Bean
        public ProductService productService() {
            return new ProductServiceImpl();
        }
    }

	@Autowired
	ProductService productService;
	
	@MockBean
    private ProductRepository productRepository;
	
	Product p;
	List<Product> list;

	@Before
	public void setUp() {
		this.p = new Product();
	}

	@Test
	public void saveProduct() {
		// Product p = new Product();
//		p.setProductName("Name");
		p.setValue(100);
		p.setId(1L);
		p.setCreditId(2);
		productService.save(p);

		Mockito.when(productRepository.findByCreditId(2)).equals(p);
//		assertThat(p.getId()).isNotNull();
	}

	@Test
	public void savedProductHasIdNameCreditIdValue() {
		p.setProductName("name");
//		assertThat(p.getProductName()).isEqualTo("name");
		Mockito.when(productRepository.findAll()).thenReturn(list);
	}
	// @AfterClass
	// public static void tearDownAfterClass() throws Exception {
	//// productService.delete(p);
	// }

}
