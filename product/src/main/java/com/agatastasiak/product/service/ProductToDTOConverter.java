package com.agatastasiak.product.service;

import com.agatastasiak.product.dto.ProductDTO;
import com.agatastasiak.product.entity.Product;

public interface ProductToDTOConverter {

	ProductDTO convertToDTO(Product product);
}
