package com.agatastasiak.product.service.impl;

import org.springframework.stereotype.Service;

import com.agatastasiak.product.dto.ProductDTO;
import com.agatastasiak.product.entity.Product;
import com.agatastasiak.product.service.ProductToDTOConverter;

@Service
public class ProductToDTOConverterImpl implements ProductToDTOConverter{

	public ProductDTO convertToDTO (Product product) {
		ProductDTO productDTO = new ProductDTO();
		productDTO.setProductName(product.getProductName());
		productDTO.setValue(product.getValue());
		return productDTO;
	}
}
