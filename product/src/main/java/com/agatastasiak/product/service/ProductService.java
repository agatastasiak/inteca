package com.agatastasiak.product.service;

import java.util.Optional;

import javax.validation.Valid;

import com.agatastasiak.product.entity.Product;

public interface ProductService {

	Optional<Product> findByCreditId(int creditId);
	Product save (Product product);
	void delete(Product product);
}
