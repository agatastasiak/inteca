package com.agatastasiak.product.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agatastasiak.product.entity.Product;
import com.agatastasiak.product.repository.ProductRepository;
import com.agatastasiak.product.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;
	
	@Override
	public Optional<Product> findByCreditId(int creditId) {
		return productRepository.findByCreditId(creditId);
	}

	@Override
	public Product save(Product product) {
		return productRepository.save(product);
	}

	@Override
	public void delete(Product product) {
		productRepository.delete(product);
		
	}

	
}
