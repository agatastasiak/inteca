package com.agatastasiak.product.dto;

import javax.persistence.Column;

public class ProductDTO {

	@Column(name = "PRODUCT_NAME")
	private String productName;

	@Column(name = "VALUE")
	private int value;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "ProductDTO [productName=" + productName + ", value=" + value + "]";
	}

}
