package com.agatastasiak.product.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.agatastasiak.product.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	Optional<Product> findByCreditId(int creditId);
	Product save(Product product);
	void delete(Product product);
}
