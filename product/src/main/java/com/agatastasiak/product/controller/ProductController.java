package com.agatastasiak.product.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.agatastasiak.product.dto.ProductDTO;
import com.agatastasiak.product.entity.Product;
import com.agatastasiak.product.exception.ProductAssignedToCreditIdNotFoundException;
import com.agatastasiak.product.service.ProductService;
import com.agatastasiak.product.service.ProductToDTOConverter;

@RestController
@RequestMapping("/products")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductToDTOConverter productToDTOConverter;
	
	@GetMapping("{creditId}")
	public ProductDTO findByCreditId(@PathVariable(name = "creditId") int creditId) {
		Optional<Product> product = this.productService.findByCreditId(creditId);
		if(!product.isPresent()) {
			throw new ProductAssignedToCreditIdNotFoundException(creditId);
		}
		return productToDTOConverter.convertToDTO(product.get());
	}
	
	@PostMapping("")
	@ResponseStatus(HttpStatus.CREATED) // 201
	public Product create(@RequestBody @Valid Product product) {
		if(product.getId() != null) {
			product.setId(null);
		}
		return productService.save(product);
	}

}
