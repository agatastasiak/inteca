package com.agatastasiak.product.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such product")
public class ProductAssignedToCreditIdNotFoundException extends RuntimeException {

	private final int customerId;
	
	public ProductAssignedToCreditIdNotFoundException(int id) {
		this.customerId = id;
	}
}
