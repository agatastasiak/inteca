package com.agatastasiak.customer.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such customer")
public class CustomerAssignedToCreditIdNotFoundException extends RuntimeException {

	private final int customerId;

	public CustomerAssignedToCreditIdNotFoundException(int id) {
		this.customerId = id;
	}
}
