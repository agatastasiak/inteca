package com.agatastasiak.customer.service;

import com.agatastasiak.customer.dto.CustomerDTO;
import com.agatastasiak.customer.entity.Customer;

public interface CustomerToDTOConverter {

	public CustomerDTO convertToDTO(Customer customer);
}
