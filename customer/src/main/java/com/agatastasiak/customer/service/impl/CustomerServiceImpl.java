package com.agatastasiak.customer.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agatastasiak.customer.dto.CustomerDTO;
import com.agatastasiak.customer.entity.Customer;
import com.agatastasiak.customer.repository.CustomerRepository;
import com.agatastasiak.customer.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerRepository customerRepository;

	@Override
	public Optional<Customer> findByCreditId(int creditId) {
		return customerRepository.findByCreditId(creditId);
	}

	@Override
	public Customer save(Customer customer) {
		return customerRepository.save(customer);
	}

}
