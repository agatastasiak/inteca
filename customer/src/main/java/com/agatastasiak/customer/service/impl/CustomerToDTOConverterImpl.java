package com.agatastasiak.customer.service.impl;

import org.springframework.stereotype.Service;

import com.agatastasiak.customer.dto.CustomerDTO;
import com.agatastasiak.customer.entity.Customer;
import com.agatastasiak.customer.service.CustomerToDTOConverter;

@Service
public class CustomerToDTOConverterImpl implements CustomerToDTOConverter {

	@Override
	public CustomerDTO convertToDTO(Customer customer) {
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setFirstName(customer.getFirstName());
		customerDTO.setSurname(customer.getSurname());
		customerDTO.setPesel(customer.getPesel());
		return customerDTO;
	}

	

}
