package com.agatastasiak.customer.service;

import java.util.Optional;

import javax.validation.Valid;

import com.agatastasiak.customer.dto.CustomerDTO;
import com.agatastasiak.customer.entity.Customer;

public interface CustomerService {

	Optional<Customer> findByCreditId(int creditId);
	Customer save(@Valid Customer customer);
}
