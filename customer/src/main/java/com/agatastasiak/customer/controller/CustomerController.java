package com.agatastasiak.customer.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.agatastasiak.customer.entity.Customer;
import com.agatastasiak.customer.exceptions.CustomerAssignedToCreditIdNotFoundException;
import com.agatastasiak.customer.service.CustomerService;

@RestController
@RequestMapping("/customers")
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@GetMapping("{creditId}")
	public Customer findById(@PathVariable(name = "creditId") int creditId) {
		Optional<Customer> customer = this.customerService.findByCreditId(creditId);
		if (!customer.isPresent()) {
			throw new CustomerAssignedToCreditIdNotFoundException(creditId);
		}

		return customer.get();
	}
	
	@PostMapping("") // how not to allow providing id in json POST
    public Customer create(@RequestBody @Valid Customer customer) {
        return customerService.save(customer);
    }


}
