package com.agatastasiak.customer.dto;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NaturalId;

public class CustomerDTO {

	@Column(name = "FIRST_NAME")
	@NotNull
	private String firstName;

	@Column(name = "SURNAME")
	@NotNull
	private String surname;

	@Column(name = "PESEL", length = 11)
	@NotNull
	@NaturalId
	private String pesel;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	@Override
	public String toString() {
		return "CustomerDTO [firstName=" + firstName + ", surname=" + surname + ", pesel=" + pesel + "]";
	}

}
