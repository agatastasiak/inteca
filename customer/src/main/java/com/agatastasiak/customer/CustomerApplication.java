package com.agatastasiak.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.agatastasiak.customer.service.impl.CustomerServiceImpl;

@SpringBootApplication
public class CustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerApplication.class, args);
		/*CustomerServiceImpl s = new CustomerServiceImpl();
		s.save(customer)*/
		
	}

}
