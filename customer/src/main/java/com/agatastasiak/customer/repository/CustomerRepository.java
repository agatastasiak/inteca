package com.agatastasiak.customer.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.agatastasiak.customer.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer,Long> {
	
	Optional<Customer> findByCreditId(int creditId);
	Customer save(Customer customer);
}
