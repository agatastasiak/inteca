package com.agatastasiak.credit.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.validation.Valid;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.agatastasiak.credit.dto.CreditDTO;
import com.agatastasiak.credit.entity.Credit;
import com.agatastasiak.credit.service.CreateCreditService;
import com.agatastasiak.credit.service.CreditService;

@RestController
@RequestMapping("/credits")
public class CreditController {

	@Autowired
	CreditService creditService;

	@Autowired
	CreateCreditService createCreditService;

	private Long createdCreditId;

	@PostMapping("")
	@ResponseStatus(HttpStatus.CREATED)
	public Long createCredit(@RequestBody @Valid Credit credit) {
		creditService.save(credit);
		JSONObject customerJson = createCreditService.create("Anna", "Kowalska", new Random().toString(),
				credit.getId());
		int statusC = createCreditService.executePost(customerJson, "http://localhost:8081/customers");
		JSONObject productJson = createCreditService.create("Product", new Random().nextInt(), credit.getId());
		int statusP = createCreditService.executePost(productJson, "http://localhost:8082/products");
		if (statusC == 201 && statusP == 201) {
			this.createdCreditId = credit.getId();
		}
		return createdCreditId;
	}

	@GetMapping("")
	@ResponseStatus(HttpStatus.OK)
	public List<Credit> findCredit() {
		List<Credit> listOfCredits = new ArrayList<>();
		listOfCredits = creditService.findAll();
		return listOfCredits;
	}

	@GetMapping("/allInfo")
	@ResponseStatus(HttpStatus.OK)
	public List<CreditDTO> allCreditInfo() throws ParseException, IOException {

		List<CreditDTO> creditList = new ArrayList<>();
		List<Credit> listOfCredits = creditService.findAll();

		for (int i = 0; i < listOfCredits.size(); i++) {
			Long creditId = listOfCredits.get(i).getId();
			JSONObject c = creditService.getRequest(creditId, "localhost:8081", "customers/");
			JSONObject p = creditService.getRequest(creditId, "localhost:8082", "products/");
			creditList.add(creditService.buildCreditDTOInfo(c, p, i, listOfCredits));
		}
		return creditList;
	}

}
