package com.agatastasiak.credit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.agatastasiak.credit.dto.CreditDTO;
import com.agatastasiak.credit.entity.Credit;

@Repository
public interface CreditRepository extends JpaRepository<Credit, Long> {

	Credit save(Credit credit);

	CreditDTO save(CreditDTO creditDTO);

	List<Credit> findAll();

}
