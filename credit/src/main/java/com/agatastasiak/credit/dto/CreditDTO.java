package com.agatastasiak.credit.dto;

import java.util.Map;

import org.json.simple.JSONObject;

public class CreditDTO {

	private Long id;

	private String creditName;

	private Map<String, String> customer;

	private Map<String, String> product;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCreditName() {
		return creditName;
	}

	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}

	public Map<String, String> getCustomer() {
		return customer;
	}

	public void setCustomer(JSONObject c) {
		this.customer = (Map<String, String>) c;
	}

	public Map<String, String> getProduct() {
		return product;
	}

	public void setProduct(JSONObject p) {
		this.product = (Map<String, String>) p;
	}

	@Override
	public String toString() {
		return "CreditDTO [id=" + id + ", creditName=" + creditName + ", customer=" + customer + ", product=" + product
				+ "]";
	}

}
