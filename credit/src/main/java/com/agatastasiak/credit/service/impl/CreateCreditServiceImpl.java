package com.agatastasiak.credit.service.impl;

import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import com.agatastasiak.credit.service.CreateCreditService;

@Service
public class CreateCreditServiceImpl implements CreateCreditService {

	/*
	 * private URI uri;
	 * 
	 * private HttpResponse httpResponse;
	 */

	public JSONObject create(String name, String surname, String pesel, Long creditId) {
		JSONObject jsonCustomer = new JSONObject();
		try {
			jsonCustomer.put("firstName", name);
			jsonCustomer.put("surname", surname);
			jsonCustomer.put("pesel", pesel);
			jsonCustomer.put("creditId", creditId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonCustomer;
	}

	public int executePost(JSONObject json, String url) {

		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost request = new HttpPost(url);
		StringEntity params;
		int status = 0;

		try {
			params = new StringEntity(json.toString());
			request.addHeader("content-type", "application/json");
			request.setEntity(params);
			try {
				status = httpClient.execute(request).getStatusLine().getStatusCode();

			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		try {
			httpClient.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return status;

	}

	@Override
	public JSONObject create(String productName, int value, Long creditId) {
		JSONObject jsonProduct = new JSONObject();
		try {
			jsonProduct.put("productName", productName);
			jsonProduct.put("value", value);
			jsonProduct.put("creditId", creditId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonProduct;
	}
}
