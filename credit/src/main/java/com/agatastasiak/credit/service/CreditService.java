package com.agatastasiak.credit.service;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.List;

import org.apache.http.HttpResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import com.agatastasiak.credit.dto.CreditDTO;
import com.agatastasiak.credit.entity.Credit;

@Service
public interface CreditService {

	List<Credit> findAll();

	Credit save(Credit credit);

	Credit save(CreditDTO creditDTO);

	CreditDTO convertToCreditDTO(Credit credit);

	Credit convertCreditDTOToEntity(CreditDTO creditDTO);

	public HttpResponse generateHttpResponse(URI uri);

	JSONObject parseHttpResponse(HttpResponse response) throws ParseException, IOException;

	public URI createURI(Long creditId, String format, String host, String path);

	public URL createURL(URI uri);

	JSONObject getRequest(Long creditId, String host, String path);

	CreditDTO buildCreditDTOInfo(JSONObject c, JSONObject p, int index, List<Credit> listCredit);

}
