package com.agatastasiak.credit.service;

import org.json.simple.JSONObject;

public interface CreateCreditService {

	JSONObject create(String name, String surname, String pesel, Long creditId);

	JSONObject create(String productName, int value, Long creditId);

	int executePost(JSONObject productJson, String url);
}
