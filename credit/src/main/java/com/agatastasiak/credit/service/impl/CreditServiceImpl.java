package com.agatastasiak.credit.service.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agatastasiak.credit.dto.CreditDTO;
import com.agatastasiak.credit.entity.Credit;
import com.agatastasiak.credit.repository.CreditRepository;
import com.agatastasiak.credit.service.CreditService;

@Service
public class CreditServiceImpl implements CreditService {

	@Autowired
	CreditRepository creditRepository;

	@Override
	public List<Credit> findAll() {
		return creditRepository.findAll();
	}

	@Override
	public Credit save(Credit credit) {
		return creditRepository.save(credit);
	}

	@Override
	public CreditDTO convertToCreditDTO(Credit credit) {
		CreditDTO creditDTO = new CreditDTO();
		creditDTO.setCreditName(credit.getCreditName());
		creditDTO.setId(credit.getId());
		return creditDTO;
	}

	@Override
	public Credit convertCreditDTOToEntity(CreditDTO creditDTO) {
		Credit credit = new Credit();
		credit.setCreditName(creditDTO.getCreditName());
		credit.setId(creditDTO.getId());
		return credit;
	}

	@Override
	public Credit save(CreditDTO creditDTO) {
		return null;
	}

	@Override
	public HttpResponse generateHttpResponse(URI uri) {

		HttpGet httpGet = new HttpGet(uri);
		HttpClient client = HttpClientBuilder.create().build();
		httpGet.setHeader("Content-Type", "application/json");
		HttpResponse response = null;

		try {
			response = client.execute(httpGet);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return response;

	}

	@Override
	public JSONObject parseHttpResponse(HttpResponse response) throws ParseException, IOException {
		BasicResponseHandler h = new BasicResponseHandler();
		JSONParser parser = new JSONParser();
		JSONObject responseToJSON = null;
		if (response.getStatusLine().getStatusCode() == 200) {

			String valueFromResponse = h.handleResponse(response);
			responseToJSON = (JSONObject) parser.parse(valueFromResponse.toString());

		}
		return responseToJSON;
	}

	@Override
	public URI createURI(Long creditId, String format, String host, String path) {
		URI uri = null;
		try {
			uri = new URIBuilder().setScheme("http").setHost(host).setPath(path + creditId)
					.setParameter("format", "json").build();

			System.out.println("URI: " + uri.toString());

		} catch (URISyntaxException e) {
			System.out.println("Exception:" + e.toString());
			e.printStackTrace();
		}
		return uri;

	}

	@Override
	public URL createURL(URI uri) {
		URL info = null;
		try {
			info = uri.toURL();
			System.out.println("URL: " + info);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return info;
	}

	public JSONObject getRequest(Long creditId, String host, String path) {
		URI uri = this.createURI(creditId, "json", host, path);
		JSONObject json = new JSONObject();
		HttpResponse httpResponse = this.generateHttpResponse(uri);
		try {
			json = this.parseHttpResponse(httpResponse);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return json;

	}

	public CreditDTO buildCreditDTOInfo(JSONObject c, JSONObject p, int index, List<Credit> listOfCredits) {
		CreditDTO creditDTO = new CreditDTO();
		Long creditId = listOfCredits.get(index).getId();
		String creditName = listOfCredits.get(index).getCreditName();
		creditDTO.setId(creditId);
		creditDTO.setCreditName(creditName);
		creditDTO.setCustomer(c);
		creditDTO.setProduct(p);
		return creditDTO;
	}

}
