package com.agatastasiak.credit.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CREDIT", schema = "DB_SCHEMA_CREDIT")
public class Credit {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CREDIT_ID")
	private Long id;

	@Column(name = "CREDIT_NAME")
	@NotNull
	private String creditName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCreditName() {
		return creditName;
	}

	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}

	@Override
	public String toString() {
		return "Credit [id=" + id + ", creditName=" + creditName + "]";
	}

}
